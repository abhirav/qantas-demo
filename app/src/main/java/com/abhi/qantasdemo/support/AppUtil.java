package com.abhi.qantasdemo.support;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;

import com.abhi.qantasdemo.AppClass;
import com.abhi.qantasdemo.R;
import com.squareup.picasso.Picasso;

public class AppUtil {

    /*
        Check for Internet Connectivity
     */
    private static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) AppClass.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } else {
            return false;
        }
    }

    public static void showError(View view) {
        Snackbar snackbar = Snackbar.make(view,
                isNetworkAvailable() ? view.getResources().getString(R.string.error_generic) : view.getResources().getString(R.string.error_no_internet),
                Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    /*
        Load image to ImageView via DataBinding tag in Xml layout files
    */
    @BindingAdapter("app:imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        if (!imageUrl.isEmpty()) {
            Picasso.with(view.getContext())
                    .load(imageUrl)
                    .placeholder(R.drawable.no_image)
                    .into(view);
        } else {
            view.setVisibility(View.GONE);
        }
    }
}
