package com.abhi.qantasdemo.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.abhi.qantasdemo.R;
import com.abhi.qantasdemo.databinding.ActivityDetailBinding;
import com.abhi.qantasdemo.model.Recipe;
import com.abhi.qantasdemo.support.Constant;

public class DetailActivity extends BaseActivity {
    private Recipe recipe;

    /*
        Set activity layout
    */
    @Override
    protected int layoutId() {
        return R.layout.activity_detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityDetailBinding binding = DataBindingUtil.setContentView(this, layoutId());
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        recipe = (Recipe) getIntent().getSerializableExtra(Constant.DATA);

        // Populate xml layout views with data through data binding
        binding.setRecipe(recipe);
    }

    /*
        Handle click events of views coming from activity_detail XMl file
     */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.recipeHrefText:
                Intent intent = new Intent(this, WebActivity.class);
                intent.putExtra(Constant.DATA, recipe.getHref());
                this.startActivity(intent);
                openActivityFromBottom();
        }
    }
}