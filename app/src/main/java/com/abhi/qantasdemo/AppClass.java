package com.abhi.qantasdemo;

import android.app.Application;
import android.content.Context;

public class AppClass extends Application {

    // Create application level variables/context here
    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
    }

    public static Context getContext() {
        return appContext;
    }
}