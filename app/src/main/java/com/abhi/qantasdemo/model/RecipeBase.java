package com.abhi.qantasdemo.model;

import java.io.Serializable;
import java.util.List;

public class RecipeBase implements Serializable {
    private String title;
    private double version;
    private String href;
    private List<Recipe> results;

    public String getTitle() {
        return title;
    }

    public double getVersion() {
        return version;
    }

    public String getHref() {
        return href;
    }

    public List<Recipe> getResults() {
        return results;
    }
}