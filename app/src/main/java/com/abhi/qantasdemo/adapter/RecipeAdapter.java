package com.abhi.qantasdemo.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abhi.qantasdemo.R;
import com.abhi.qantasdemo.model.Recipe;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecipeAdapter extends GenericRecyclerViewAdapter<Recipe> {

    private final Context context;
    public static final int HEADER = 0;
    private static final int LIST_ITEM = 1;
    public static final int GRID_ITEM = 2;

    public RecipeAdapter(Context context, List<Recipe> data, OnViewHolderClick viewHolderClick) {
        super(context, viewHolderClick);
        this.context = context;
        setList(data);
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (viewType) {
            case HEADER:
                return inflater.inflate(R.layout.recipe_item_header_view, viewGroup, false);
            case GRID_ITEM:
                return inflater.inflate(R.layout.recipe_item_grid_view, viewGroup, false);
            case LIST_ITEM:
            default:
                return inflater.inflate(R.layout.recipe_item_list_view, viewGroup, false);
        }
    }

    @Override
    protected void bindView(final Recipe recipe, final GenericRecyclerViewAdapter.ViewHolder holder) {
        switch (holder.getItemViewType()) {
            case HEADER:
                if (recipe != null) {
                    ((TextView) holder.getView(R.id.recipeHeader)).setText(recipe.getHeaderTitle());
                }
                break;
            case GRID_ITEM:
                if (recipe != null) {
                    ((TextView) holder.getView(R.id.recipeTitle)).setText(recipe.getTitle());
                    if (!TextUtils.isEmpty(recipe.getThumbnail())) {
                        Picasso.with(context)
                                .load(recipe.getThumbnail())
                                .placeholder(R.drawable.no_image)
                                .error(R.drawable.no_image)
                                .into((ImageView) holder.getView(R.id.recipeImageView));
                    }
                }
                break;
            case LIST_ITEM:
                if (recipe != null) {
                    ((TextView) holder.getView(R.id.recipeTitle)).setText(recipe.getTitle());
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (getList().get(position).isHeader())
            return HEADER;
        else if (TextUtils.isEmpty(getList().get(position).getThumbnail()))
            return LIST_ITEM;
        else
            return GRID_ITEM;
    }
}