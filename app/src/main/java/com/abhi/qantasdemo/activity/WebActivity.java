package com.abhi.qantasdemo.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.abhi.qantasdemo.R;
import com.abhi.qantasdemo.databinding.ActivityWebBinding;
import com.abhi.qantasdemo.support.AppUtil;
import com.abhi.qantasdemo.support.Constant;

public class WebActivity extends BaseActivity {

    /*
        Set activity layout
    */
    @Override
    protected int layoutId() {
        return R.layout.activity_web;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActivityWebBinding binding = DataBindingUtil.setContentView(this, layoutId());
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        String url = getIntent().getStringExtra(Constant.DATA);
        if (!TextUtils.isEmpty(url)) {

            // Load url in in-app browser
            binding.webView.setWebChromeClient(new WebChromeClient() {
                @Override
                public void onReceivedTitle(WebView view, String title) {
                    getSupportActionBar().setTitle(title);
                }

                public void onProgressChanged(WebView view, int progress) {
                    if (progress == 100)
                        binding.loading.setVisibility(View.GONE);
                }
            });
            binding.webView.setWebViewClient(new WebViewClient());
            binding.webView.loadUrl(url);
        } else {
            AppUtil.showError(binding.getRoot());
        }
    }
}