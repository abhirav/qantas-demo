package com.abhi.qantasdemo.service;

import android.text.TextUtils;

import com.abhi.qantasdemo.AppClass;
import com.abhi.qantasdemo.R;
import com.abhi.qantasdemo.model.Recipe;
import com.abhi.qantasdemo.model.RecipeBase;
import com.abhi.qantasdemo.support.Constant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
    This class handles all services relating to Recipes
    Currently there is just one service, getRecipes()
*/
public abstract class GetRecipesService {

    private final Retrofit retrofit;

    protected GetRecipesService() {
        Gson gson = new GsonBuilder().create();
        retrofit = new Retrofit.Builder()
                .baseUrl(Constant.HOST)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public void getRecipes() {
        onInit();
        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
        Call<RecipeBase> call = retrofitInterface.getRecipes();
        call.enqueue(new Callback<RecipeBase>() {
            @Override
            public void onResponse(Call<RecipeBase> call, Response<RecipeBase> response) {
                if (response.body() != null
                        && response.body().getResults() != null) {

                    // Sort: With thumbnails on top and slide remaining ones to bottom of list
                    Collections.sort(response.body().getResults(), new Comparator<Recipe>() {
                        @Override
                        public int compare(Recipe recipe1, Recipe recipe2) {

                            // Don't lose relative position of items which has no thumbnail
                            if (recipe1.getThumbnail().isEmpty() && recipe2.getThumbnail().isEmpty()) {
                                return 1;
                            }
                            return recipe2.getThumbnail().isEmpty() ? -1 : 1;
                        }
                    });

                    // Add "Popular" Header as 1st item
                    response.body().getResults().add(0, new Recipe(true, AppClass.getContext().getString(R.string.recipe_popular_header)));

                    // Add "Other" Header just after last item with thumbnail and before first item without thumbnail
                    for (int i = 1; i < response.body().getResults().size(); i++) {
                        if (TextUtils.isEmpty(response.body().getResults().get(i).getThumbnail())) {
                            response.body().getResults().add(i, new Recipe(true, AppClass.getContext().getString(R.string.recipe_other_header)));
                            break;
                        }
                    }

                    onSuccess(response.body());
                } else {
                    onFail();
                }
            }

            @Override
            public void onFailure(Call<RecipeBase> call, Throwable t) {
                onFail();
            }
        });
    }

    public abstract void onInit();

    public abstract void onSuccess(RecipeBase recipeBase);

    /*
        Can modify onFail method to handle:
            1. Top level exceptions
            2. Service level exceptions, etc.
     */
    public abstract void onFail();
}