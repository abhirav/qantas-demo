package com.abhi.qantasdemo.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;

import com.abhi.qantasdemo.R;
import com.abhi.qantasdemo.adapter.GenericRecyclerViewAdapter;
import com.abhi.qantasdemo.adapter.RecipeAdapter;
import com.abhi.qantasdemo.databinding.ActivityMainBinding;
import com.abhi.qantasdemo.model.RecipeBase;
import com.abhi.qantasdemo.service.GetRecipesService;
import com.abhi.qantasdemo.support.AppUtil;
import com.abhi.qantasdemo.support.Constant;

public class MainActivity extends BaseActivity implements GenericRecyclerViewAdapter.OnViewHolderClick {
    private ActivityMainBinding binding;
    private RecipeAdapter recipeAdapter;

    /*
      Set activity layout
    */
    @Override
    protected int layoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, layoutId());

        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRecipeData();
            }
        });

        getRecipeData();
    }

    private void getRecipeData() {

        // Call Recipe service and fetch the recipes data
        new GetRecipesService() {

            @Override
            public void onInit() {
                binding.swipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onSuccess(RecipeBase recipeBase) {
                binding.swipeRefreshLayout.setRefreshing(false);
                recipeAdapter = new RecipeAdapter(MainActivity.this, recipeBase.getResults(), MainActivity.this);
                setOrRefreshGridLayoutManager();
                binding.recipeRecyclerView.setAdapter(recipeAdapter);
            }

            @Override
            public void onFail() {
                binding.swipeRefreshLayout.setRefreshing(false);
                AppUtil.showError(binding.getRoot());
            }
        }.getRecipes();
    }

    /*
        Handle grid layout for Header, List and Grid view of recipes items in adapter
     */
    private void setOrRefreshGridLayoutManager() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(MainActivity.this, getResources().getInteger(R.integer.grid_span));
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (recipeAdapter.getItemViewType(position)) {
                    case RecipeAdapter.GRID_ITEM:
                        return 1;
                    default:
                        return getResources().getInteger(R.integer.grid_span);
                }
            }
        });
        binding.recipeRecyclerView.setLayoutManager(gridLayoutManager);
    }

    /*
        Get recipe item position from RecyclerView Recipe item click and navigate to details screen
    */
    @Override
    public void onItemClick(int position) {

        // Header section click not allowed
        if (recipeAdapter.getItemViewType(position) != RecipeAdapter.HEADER) {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(Constant.DATA, recipeAdapter.getList().get(position));
            this.startActivity(intent);
            openActivityFromRight();
        }
    }

    /*
        Portrait mode will show grid layout with 2 columns
        Landscape mode will show grid layout with 3 columns

        Columns are defined in integer.xml file in values and values-land resource directory
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setOrRefreshGridLayoutManager();
    }
}