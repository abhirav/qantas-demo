# Qantas Demo

Qantas Demo app uses following 3rd party libraries.

# Picasso (http://square.github.io/picasso/)
-	To lazy load recipe images

# Retrofit (http://square.github.io/retrofit/)
-	for making HTTP URL connection

This demo app has three screens. 

1. MainActivity - which displays recyclerview having three view types, Header, List and Grid. This screen supports portrait and landscape mode.
2. DetailActivity - displays detailed data about Recipe
3. WebActivity - In app browser, loading recipe URL

-	Data binding has been used for data injection. No more findViewByIds.
-	Project structure is supporting Kotlin however the demo has been done using JAVA language.
-	A Generic recyclerview has been implemented to demonstrate skills about handling complex and frequently changing recyclerview requirements
-	Use of Android SDK files and resource directory like dimens, values, values-land, styles, strings has been demonstrated
-	Used Base class implementation pattern to have common animation and method calls for growing app
-	Easily understandable folder structure has been created.