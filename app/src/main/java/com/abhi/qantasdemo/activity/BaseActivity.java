package com.abhi.qantasdemo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.abhi.qantasdemo.R;

/*
    Base activity of all activities
    This activity allows to perform common stuffs from activities extending BaseActivity
 */
public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutId());
    }

    protected abstract int layoutId();

    /*
     * slide activity from right
     */
    protected void openActivityFromRight() {
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
    }

    /*
     * slide activity from right
     */
    private void openActivityFromLeft() {
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    /*
     * slide activity from bottom
     */
    protected void openActivityFromBottom() {
        overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
    }

    /*
     * slide activity from top
     */
    private void openActivityFromTop() {
        overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (this instanceof WebActivity)
            openActivityFromTop();
        else
            openActivityFromLeft();
    }
}