package com.abhi.qantasdemo.model;

import java.io.Serializable;

public class Recipe implements Serializable {
    private String title;
    private String href;
    private String ingredients;
    private String thumbnail;
    private final boolean isHeader;
    private final String headerTitle;

    public Recipe(boolean isHeader, String headerTitle) {
        this.isHeader = isHeader;
        this.headerTitle = headerTitle;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public String getHeaderTitle() {
        return headerTitle.trim();
    }

    public String getTitle() {
        return title.trim();
    }

    public String getHref() {
        return href.trim();
    }

    public String getIngredients() {
        return ingredients.trim();
    }

    public String getThumbnail() {
        return thumbnail.trim();
    }
}
