package com.abhi.qantasdemo.service;

import com.abhi.qantasdemo.model.RecipeBase;

import retrofit2.Call;
import retrofit2.http.GET;

interface RetrofitInterface {

    @GET("recipes.json")
    Call<RecipeBase> getRecipes();
}