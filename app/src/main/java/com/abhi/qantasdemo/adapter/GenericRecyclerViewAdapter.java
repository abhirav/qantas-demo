package com.abhi.qantasdemo.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
    This is Generic RecyclerView Adapter which accommodate most of the use cases.
    Using this will make it easy if the child adapter want to add items to list, remove, alter contents, etc.
    This adapter takes care of binding and creating ViewHolders
 */

public abstract class GenericRecyclerViewAdapter<T> extends RecyclerView.Adapter<GenericRecyclerViewAdapter.ViewHolder> {
    private List<T> list = Collections.emptyList();
    private final Context context;
    private final OnViewHolderClick listener;

    GenericRecyclerViewAdapter(Context context, OnViewHolderClick listener) {
        super();
        this.context = context;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final Map<Integer, View> map;
        private final OnViewHolderClick mListener;

        private ViewHolder(View view, OnViewHolderClick listener) {
            super(view);
            map = new HashMap<>();
            map.put(0, view);
            mListener = listener;

            if (mListener != null) {
                view.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View view) {
            if (mListener != null)
                mListener.onItemClick(getAdapterPosition());
        }

        private void initViewById(int id) {
            View view = (getView() != null ? getView().findViewById(id) : null);
            if (view != null)
                map.put(id, view);
        }

        public View getView() {
            return getView(0);
        }

        View getView(int id) {
            if (map.containsKey(id))
                return map.get(id);
            else
                initViewById(id);

            return map.get(id);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new ViewHolder(createView(context, viewGroup, viewType), listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        bindView(getItem(position), viewHolder);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private T getItem(int index) {
        return ((list != null && index < list.size()) ? list.get(index) : null);
    }

    void setList(List<T> list) {
        this.list = list;
    }

    // Some methods you can implement commonly are
    // 1. public void deleteItem(T item)
    // 2. public void refreshList(List<T> list)
    // 3. public void addItem(T item), etc

    public List<T> getList() {
        return list;
    }

    public interface OnViewHolderClick {
        void onItemClick(int position);
    }

    protected abstract View createView(Context context, ViewGroup viewGroup, int viewType);

    protected abstract void bindView(T item, ViewHolder viewHolder);
}